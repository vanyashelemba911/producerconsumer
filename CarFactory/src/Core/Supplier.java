package Core;

import java.util.Random;
import java.util.ArrayList;

import Core.Logger.ILogger;
import Core.Details.Detail;
import Core.Logger.DefaultOut;
import Core.Factories.IDetailFactory;

public class Supplier<Item extends Detail> implements IStartable
{
    private static final Object SyncWhenCreating = new Object();

    private static volatile int _id = 0;
    private int ID;

    private SimulationState CurrentState;
    private Thread SupplierThread;

    private float MakingProgress;
    private int DetailMakingTime;
    private int FinishedDetailsCount;

    private Storage<Item> ItemsStorage;
    private IDetailFactory ItemsFactory;

    private ArrayList<Runnable> NotificationsWhenSupply;
    private ArrayList<Runnable> NotificationsWhenCreating;

    private ILogger Log;

    private Object WhenPause;

    //region Ctor
    public Supplier(Storage<Item> someStorage, IDetailFactory someFactory)
    {
        Initialize(someStorage, someFactory, 200);
    }

    public Supplier(Storage<Item> someStorage, IDetailFactory someFactory, int detailMakingTime)
    {
        Initialize(someStorage, someFactory, detailMakingTime);
    }

    //endregion

    //region Init
    private void Initialize(Storage<Item> someStorage, IDetailFactory someFactory, int detailMakingTime)
    {
        if(someStorage == null || someFactory == null)
            throw new ExceptionInInitializerError("someStorage == null || someFactory == null");
        if(detailMakingTime < 0)
            throw new ExceptionInInitializerError("detailMakingTime < 0");

        synchronized(SyncWhenCreating)
        {
            ID = _id++;
        }

        ItemsStorage = someStorage;
        ItemsFactory = someFactory;
        DetailMakingTime = detailMakingTime;

        CurrentState = SimulationState.ssStopped;

        NotificationsWhenSupply = new ArrayList<>();
        NotificationsWhenCreating = new ArrayList<>();

        WhenPause = new Object();
    }

    //endregion

    //region Additional methods
    private void OnCreate()
    {
        for(Runnable SomeCommand : NotificationsWhenCreating)
            SomeCommand.run();
    }

    private void OnSupply()
    {
        for(Runnable SomeCommand : NotificationsWhenSupply)
            SomeCommand.run();
    }

    //endregion

    //region LogWriter
    private void WriteToLog(String msg)
    {
        if( Log != null )
            Log.Write(msg);
        else
            DefaultOut.Write( msg );
    }

    //endregion

    //region Logic
    private Item GenerateItem() throws Exception
    {
        Random SeedGenerator = new Random();

        int Ticks = 10;
        int DetailMakingTick = DetailMakingTime / Ticks;

        MakingProgress = 0.0F;

        for(int i = 0; i < Ticks; i++)
        {
            if( CurrentState == SimulationState.ssPaused )
            {
                synchronized(WhenPause)
                {
                    WhenPause.wait();
                }

                Log.Write("Supplier#" + ID + " work resumed.");
            }

            MakingProgress += 100.0 / Ticks;

            OnCreate();

            Thread.sleep(DetailMakingTick);
        }

        return (Item) ItemsFactory.Generate( 1000 + SeedGenerator.nextInt(9000) );
    }

    private void MakeProduct()
    {
        try
        {
            Item SomeItem;

            while(true)
            {
                SomeItem = GenerateItem();

                ItemsStorage.PutItem(SomeItem);

                WriteToLog("Supplier #" + ID + " Put item: " + SomeItem.toString());

                FinishedDetailsCount++;

                OnSupply();
            }
        }
        catch(Exception SomeException)
        {
            WriteToLog( "Exception in Supplier#" + ID + " <" + ItemsFactory.toString() + "> " + SomeException.getMessage());
        }
    }

    //endregion

    //region Interface

    @Override
    public void Start()
    {
        if( CurrentState == SimulationState.ssStopped )
        {
            SupplierThread = new Thread(this::MakeProduct);

            CurrentState = SimulationState.ssRunning;

            SupplierThread.start();
        }
    }

    @Override
    public void Pause()
    {
        if( CurrentState == SimulationState.ssRunning )
        {
            Log.Write("Supplier#" + ID + " work paused.");

            CurrentState = SimulationState.ssPaused;
        }
    }

    @Override
    public void Resume()
    {
        if( CurrentState == SimulationState.ssPaused )
        {
            CurrentState = SimulationState.ssRunning;

            synchronized(WhenPause)
            {
                WhenPause.notifyAll();
            }
        }
    }

    @Override
    public void Stop()
    {
        if( CurrentState != SimulationState.ssStopped )
        {
            SupplierThread.stop();

            CurrentState = SimulationState.ssStopped;

            MakingProgress = 0;
            FinishedDetailsCount = 0;

            NotificationsWhenSupply = new ArrayList<>();
            NotificationsWhenCreating = new ArrayList<>();
        }
    }

    public void SetDebugLog( ILogger log )
    {
        if( log == null )
            throw new IllegalArgumentException("SetDebugLog()");

        Log = log;
    }

    public void SetMakingTime(int makingTime)
    {
        if(makingTime < 1) throw new IllegalArgumentException("DetailMakingTime must be greater than 1");

        DetailMakingTime = makingTime;
    }

    public void WhenCreating(Runnable someCommand)
    {
        if(someCommand == null) throw new IllegalArgumentException("RunWhenCreating(null)");

        NotificationsWhenCreating.add(someCommand);
    }

    public void WhenSupply(Runnable someCommand)
    {
        if(someCommand == null) throw new IllegalArgumentException("RunWhenSupply(null)");

        NotificationsWhenSupply.add(someCommand);
    }

    public int GetID() { return ID; }
    public float GetMakingProgress() { return MakingProgress; }
    public int GetFinishedDetailsCount() { return FinishedDetailsCount; }

    //endregion
}