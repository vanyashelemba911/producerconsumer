package Core.Logger;

import java.io.File;
import java.util.Date;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.concurrent.ConcurrentLinkedQueue;

public class FileLoggerAsync implements ILogger
{
    private int MaxCacheSize = 100;

    private SimpleDateFormat DateFormatter;

    private ConcurrentLinkedQueue<String> Messages;

    private String FilePath;
    private String FileName;
    private String LogFolder;
    private String LogFileExt;

    public FileLoggerAsync()
    {
        Messages = new ConcurrentLinkedQueue<String>();
        DateFormatter = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss.SSS");

        LogFolder = "Logs\\";
        LogFileExt = ".log";
        FileName = (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss")).format(new Date(System.currentTimeMillis()));

        FilePath =  LogFolder + FileName + LogFileExt;

        File Folder = new File(LogFolder);
        if(!Folder.exists())
        {
            Folder.mkdir();
        }

        File LogFile = new File(FilePath);
        if( !LogFile.exists() )
        {
            try
            {
                LogFile.createNewFile();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    public synchronized void SaveLogToFile()
    {
        try(FileWriter Out = new FileWriter(FilePath, true))
        {
            while(Messages.size() > 0)
            {
                Out.append(Messages.poll() + "\n");
            }

            Out.flush();
        }
        catch(IOException ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    public boolean SetMaxCacheSize( int Size )
    {
        if( Size < 0 ) return false;

        MaxCacheSize = Size;

        return true;
    }

    public void Write(String msg)
    {
        Messages.offer( DateFormatter.format(new Date(System.currentTimeMillis())) + " -> " + msg);

        if(Messages.size() > MaxCacheSize)
            (new Thread(this::SaveLogToFile)).start();
    }
}