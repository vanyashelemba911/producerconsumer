package Core;

import Core.Logger.ILogger;
import Core.Logger.DefaultOut;
import javafx.beans.binding.When;

import java.util.ArrayList;

public class Dealer implements IStartable
{
    private static volatile int _id = 0;
    private int ID;

    private int RequestRate;
    private Storage<Car> CarStorage;

    private int BoughtCarsCount;

    private ArrayList<Runnable> WhenBuy;

    private Thread DealerThread;

    private ILogger Log;

    private SimulationState CurrentState;

    private Object WhenPause;

    //region Ctor
    public Dealer( Storage<Car> carStorage )
    {
        Initialize(carStorage, 100);
    }
    public Dealer( Storage<Car> carStorage, int requestRate )
    {
        Initialize(carStorage, requestRate);
    }
    //endregion

    //region Init
    private void Initialize(Storage<Car> carStorage, int requestRate)
    {
        if(carStorage == null || requestRate < 1) throw new IllegalArgumentException("carStorage == null || requestRate < 1");

        ID = _id++;

        RequestRate = requestRate;
        CarStorage = carStorage;

        BoughtCarsCount = 0;

        CurrentState = SimulationState.ssStopped;

        WhenPause = new Object();

        WhenBuy = new ArrayList<>();
    }
    //endregion

    //region LogWriter
    private void WriteToLog(String msg)
    {
        if( Log != null )
            Log.Write(msg);
        else
            DefaultOut.Write( msg );
    }

    //endregion

    //region etc
    private void OnPropertyChanged( ArrayList<Runnable> commands )
    {
        if(commands == null) return;

        for( Runnable command : commands ) command.run();
    }
    //endregion

    //region logic

    private void DealerWork()
    {
        Car SomeCar;

        try
        {
            while(true)
            {
                if( CurrentState == SimulationState.ssPaused )
                {
                    synchronized( WhenPause )
                    {
                        WhenPause.wait();
                    }
                }

                SomeCar = CarStorage.TakeItem();

                BoughtCarsCount++;

                WriteToLog("The Dealer#" + ID + " bought a car: " + SomeCar.toString());

                OnPropertyChanged(WhenBuy);

                Thread.sleep(RequestRate);
            }
        }
        catch(Exception SomeException)
        {
            WriteToLog(SomeException.getMessage());
        }
    }

    //endregion

    //region interface
    public void SetRequestRate(int requestRate)
    {
        if(requestRate < 1) throw new IllegalArgumentException("requestRate must be greater than 0");

        RequestRate = requestRate;
    }

    public void SetDebugLog( ILogger log )
    {
        if( log == null )
            throw new IllegalArgumentException("SetDebugLog()");

        Log = log;
    }

    public int GetID(){ return ID; }
    public int GetBoughtCarsCount(){ return BoughtCarsCount; }

    public void NotifyWhenBuy( Runnable command )
    {
        if( command == null ) throw new IllegalArgumentException("Dealer: command == null");

        WhenBuy.add(command);
    }

    @Override
    public void Start()
    {
        if( CurrentState == SimulationState.ssStopped )
        {
            BoughtCarsCount = 0;

            CurrentState = SimulationState.ssRunning;

            DealerThread = new Thread(this::DealerWork);

            DealerThread.start();
        }
    }

    @Override
    public void Pause()
    {
        if( CurrentState == SimulationState.ssRunning )
        {
            CurrentState = SimulationState.ssPaused;
        }
    }

    @Override
    public void Resume()
    {
        if( CurrentState == SimulationState.ssPaused )
        {
            CurrentState = SimulationState.ssRunning;

            synchronized(WhenPause)
            {
                WhenPause.notifyAll();
            }
        }
    }

    @Override
    public void Stop()
    {
        if( CurrentState != SimulationState.ssStopped )
        {
            DealerThread.stop();

            CurrentState = SimulationState.ssStopped;
        }
    }

    //endregion
}
