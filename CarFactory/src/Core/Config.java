package Core;

import Core.Logger.DefaultOut;
import Core.Logger.FileLoggerAsync;
import Core.Logger.ILogger;

import java.io.*;

public class Config implements Serializable
{
    //region Fields
    public int CarStorageSize;
    public int BodyStorageSize;
    public int EngineStorageSize;
    public int AccessoriesStorageSize;

    public int BodySupplierNum;
    public int EngineSupplierNum;
    public int AccessoriesSupplierNum;

    public int ManufacturerConveyorsNum;

    public int DealersNum;

    public int BodyMakingTime;
    public int EngineMakingTime;
    public int AccessoriesMakingTime;

    public int CarMakingTime;

    public int DealerRequestRate;

    //endregion

    public static Config ReadFromFile(String FileName)
    {
        Config SomeConfig;

        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream( FileName )))
        {
            SomeConfig = (Config)ois.readObject();
        }
        catch(Exception ex)
        {
            DefaultOut.Write(ex.getMessage());
            SomeConfig = GetStandardConfig();
        }
        return SomeConfig;
    }

    public void SaveToFile(String FileName)
    {

        try(ObjectOutputStream OutStream = new ObjectOutputStream(new FileOutputStream(FileName)))
        {
            OutStream.writeObject(this);
        }
        catch(Exception ex)
        {
            DefaultOut.Write(ex.getMessage());
        }
    }

    public static Config GetStandardConfig()
    {
        Config SomeConfig = new Config();

            SomeConfig.BodyMakingTime = 2000;
            SomeConfig.EngineMakingTime = 2200;
            SomeConfig.AccessoriesMakingTime = 2400;

            SomeConfig.CarStorageSize = 10;
            SomeConfig.BodyStorageSize = 20;
            SomeConfig.EngineStorageSize = 20;
            SomeConfig.AccessoriesStorageSize = 20;

            SomeConfig.BodySupplierNum = 2;
            SomeConfig.EngineSupplierNum = 2;
            SomeConfig.AccessoriesSupplierNum = 2;

            SomeConfig.ManufacturerConveyorsNum = 4;

            SomeConfig.DealersNum = 2;

            SomeConfig.CarMakingTime = 4000;

            SomeConfig.DealerRequestRate = 2000;

        return SomeConfig;
    }

    @Override
    public String toString()
    {
        return "Config:\n{\n\tBodyMakingTime : " + BodyMakingTime + ";\n" +
                "\tEngineMakingTime : " + EngineMakingTime + ";\n" +
                "\tAccessoriesMakingTime : " + AccessoriesMakingTime + ";\n" +
                "\tCarStorageSize : " + CarStorageSize + ";\n" +
                "\tBodyStorageSize : " + BodyStorageSize + ";\n" +
                "\tEngineStorageSize : " + EngineStorageSize + ";\n" +
                "\tAccessoriesStorageSize : " + AccessoriesStorageSize + ";\n" +
                "\tBodySupplierNum : " + BodySupplierNum + ";\n" +
                "\tEngineSupplierNum : " + EngineSupplierNum + ";\n" +
                "\tAccessoriesSupplierNum : " + AccessoriesSupplierNum + ";\n" +
                "\tManufacturerConveyorsNum : " + ManufacturerConveyorsNum + ";\n" +
                "\tDealersNum : " + DealersNum + ";\n" +
                "\tCarMakingTime : " + CarMakingTime + ";\n" +
                "\tDealerRequestRate : " + DealerRequestRate + ";\n}";
    }

}