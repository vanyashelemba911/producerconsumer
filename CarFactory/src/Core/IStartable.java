package Core;

public interface IStartable
{
    public void Start();
    public void Pause();
    public void Resume();
    public void Stop();
}