package Core;

import Core.Details.Body;
import Core.Details.Engine;
import Core.Details.Accessories;

public class Car
{
    private static volatile int _id = 0;
    private int ID;

    Engine CurrentEngine;
    Body CurrentBody;
    Accessories CurrentAccessories;

    public Car(Body body, Engine engine, Accessories accessories)
    {
        ID = _id++;

        CurrentEngine = engine;
        CurrentBody = body;
        CurrentAccessories = accessories;
    }

    @Override
    public String toString()
    {
        return "Car#"
                + ID
                + " [ "
                + CurrentEngine.toString()
                + ", "
                + CurrentBody.toString()
                + ", "
                + CurrentAccessories.toString()
                + " ] ";
    }
}
