package Core;

public enum SimulationState
{
    ssRunning,
    ssStopped,
    ssPaused
}
