package Core;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Storage<Item>
{
    private static final Object SyncWhenCreating = new Object();

    private static volatile int _id = 0;
    private int ID;

    private int MaxSize;
    private Class<Item> ItemType;
    private ConcurrentLinkedQueue<Item> Items;

    private Object PutMonitor;
    private Object TakeMonitor;

    private Semaphore PutSemaphore;
    private Semaphore TakeSemaphore;

    private ArrayList<Runnable> NotificationsWhenPut;
    private ArrayList<Runnable> NotificationsWhenTake;

    //region Ctor
    public Storage(Class<Item> typeOfItem)
    {
        Initialize(20,typeOfItem);
    }

    public Storage(int maxSize, Class<Item> typeOfItem)
    {
        Initialize(maxSize,typeOfItem);
    }

    //endregion

    //region Initialize
    private void Initialize(int maxSize, Class<Item> typeOfItem)
    {
        if(maxSize <= 0 || typeOfItem == null) throw new IllegalArgumentException("Storage initialization failed");

        synchronized(SyncWhenCreating)
        {
            ID = _id++;
        }

        MaxSize = maxSize;
        ItemType = typeOfItem;
        Items = new ConcurrentLinkedQueue<>();

        PutMonitor = new Object();
        TakeMonitor = new Object();

        PutSemaphore = new Semaphore(1);
        TakeSemaphore = new Semaphore(1);

        NotificationsWhenPut = new ArrayList<>();
        NotificationsWhenTake = new ArrayList<>();
    }

    //endregion

    //region AdditionalMethods
    private void OnPropertyChanged(ArrayList<Runnable> commands)
    {
        for(Runnable Command : commands)
            Command.run();
    }

    //endregion

    //region Interface
    public int GetItemCount() { return Items.size(); }

    public int GetMaxSize() { return MaxSize; }

    private boolean CanPutItem()
    {
        return Items != null && Items.size() < MaxSize;
    }

    private boolean CanTakeItem()
    {
        return Items != null && Items.size() > 0;
    }

    public void PutItem(Item SomeItem) throws Exception
    {
        if(SomeItem == null) throw new IllegalArgumentException("PutItem(null)");
        if(SomeItem.getClass() != ItemType)
            throw new IllegalArgumentException("Illegal item type! Expected: " + ItemType.toString() + " received: " + SomeItem.getClass().toString());

        PutSemaphore.acquire();

        if(!CanPutItem())
        {
            synchronized(TakeMonitor)
            {
                TakeMonitor.wait();
            }
        }

        Items.offer(SomeItem);

        OnPropertyChanged(NotificationsWhenPut);

        synchronized(PutMonitor)
        {
            PutMonitor.notify();
        }

        PutSemaphore.release();
    }

    public Item TakeItem() throws Exception
    {
        TakeSemaphore.acquire();

        if(!CanTakeItem())
        {
            synchronized(PutMonitor)
            {
                PutMonitor.wait();
            }
        }

        Item SomeItem = Items.poll();

        OnPropertyChanged(NotificationsWhenTake);

        synchronized(TakeMonitor)
        {
            TakeMonitor.notify();
        }

        TakeSemaphore.release();

        return SomeItem;
    }

    public void Reinitialize()
    {
        NotificationsWhenTake.clear();
        NotificationsWhenPut.clear();
        Items.clear();
    }

    public void SubscribeForUpdatesWhenTake(Runnable someCommand)
    {
        if(someCommand == null) throw new IllegalArgumentException("SubscribeOnTake(null)");

        NotificationsWhenTake.add(someCommand);
    }

    public void SubscribeForUpdatesWhenPut(Runnable someCommand)
    {
        if(someCommand == null) throw new IllegalArgumentException("SubscribeOnPut(null)");

        NotificationsWhenPut.add(someCommand);
    }

    //endregion
}