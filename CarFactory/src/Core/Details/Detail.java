package Core.Details;

public abstract class Detail
{
    public float Width;  // cm
    public float Height; // cm
    public float Length; // cm
    public float Weight; // kg
}
