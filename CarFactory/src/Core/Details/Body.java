package Core.Details;

import Core.Details.Enums.CarBodyType;
import Core.Details.Enums.Color;

public class Body extends Detail
{
    private volatile static int _id = 0;
    private int ID;

    private static final Object SyncObject = new Object();

    public Color bColor;
    public CarBodyType bType;

    //region Ctor
    public Body()
    {
        synchronized (SyncObject)
        {
            ID = _id++;
        }
    }
    //endregion

    public int GetID() { return ID; }

    //region prototypes
    public static Body StandardBody()
    {
        Body SomeBody = new Body();

        SomeBody.Weight = 700.0F;
        SomeBody.Length = 200.0F;
        SomeBody.Width  = 160.0F;
        SomeBody.Height = 150.0F;

        SomeBody.bType = CarBodyType.tSedan;
        SomeBody.bColor = Color.clBlack;

        return SomeBody;
    }
    //endregion

    @Override
    public String toString()
    {
        return "Body#"+ID+"[ Type:"+ bType + ", Color:" + bColor.toString() +" ]";
    }
}
