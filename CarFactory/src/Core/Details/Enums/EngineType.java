package Core.Details.Enums;

public enum EngineType
{
    tSingleCylinder,
    tVEngine,
    tWEngine,
    tRotaryEngine
}
