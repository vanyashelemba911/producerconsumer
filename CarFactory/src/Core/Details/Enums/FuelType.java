package Core.Details.Enums;

public enum FuelType
{
    tGas,
    tGasoline,
    tDiesel
}