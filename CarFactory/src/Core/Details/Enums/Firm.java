package Core.Details.Enums;

public enum Firm
{
    Bosch,
    Form,
    Volvo,
    Ford,
    Volkswagen
}
