package Core.Details.Enums;

public enum CarBodyType
{
    tSedan,
    tCoupe,
    tHatchback,
    tPickup,
    tMinivan
}
