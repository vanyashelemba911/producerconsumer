package Core.Details;

import Core.Details.Enums.EngineType;
import Core.Details.Enums.FuelType;

import java.util.Random;

public class Engine extends Detail
{
    private volatile static int _id = 0;
    private int ID;

    private static final Object SyncObject = new Object();

    public EngineType eType;
    public FuelType eFuel;
    public String eName;
    public float ePower;
    public float eCapacity;

    public Engine()
    {
        synchronized (SyncObject)
        {
            ID = _id++;
        }
    }

    public int GetID() { return ID; }

    @Override
    public String toString()
    {
        return "Engine#"+ID+"[ name="+eName+", type="+eType+", fuel="+eFuel+", power="+ePower+", cap="+eCapacity+" ];";
    }

    //region prototypes

    // Паттерн прототип - https://metanit.com/sharp/patterns/2.4.php

    public static Engine StandardDieselEngine()
    {
        Engine SomeEngine = StandardEngine();
        Random rand = new Random();

            SomeEngine.eFuel = FuelType.tDiesel;
            SomeEngine.eName = "StandardDieselEngine_#" + SomeEngine.ID;
            SomeEngine.Weight = 400F + rand.nextInt(300) + rand.nextFloat();

        return SomeEngine;
    }

    public static Engine StandardEngine()
    {
        Engine SomeEngine = new Engine();
        Random rand = new Random();

            SomeEngine.Weight = 300F;
            SomeEngine.Width  =  60F + rand.nextInt( 24) + rand.nextFloat();  // standard sizes between 60.0 and 84.0 cm
            SomeEngine.Height =  68F + rand.nextInt( 13) + rand.nextFloat(); // standard sizes between 68.58 and 81.0 cm
            SomeEngine.Length =  68F + rand.nextInt( 20) + rand.nextFloat(); // standard sizes between 68.58 and 88.58 cm

            SomeEngine.eType = EngineType.tWEngine;
            SomeEngine.eFuel = FuelType.tGasoline;
            SomeEngine.eName = "StandardEngine_#" + SomeEngine.ID;
            SomeEngine.eCapacity = 1.9F;
            SomeEngine.ePower = 104.0F;

        return SomeEngine;
    }
    //endregion
}
