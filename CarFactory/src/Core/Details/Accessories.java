package Core.Details;

import Core.Details.Enums.CarBodyType;
import Core.Details.Enums.Firm;

public class Accessories extends Detail
{
    private volatile static int _id = 0;
    private int ID;

    private static final Object SyncObject = new Object();

    public Firm aFirm;
    public CarBodyType aType;

    public Accessories()
    {
        synchronized(SyncObject)
        {
            ID = _id++;
        }
    }

    public int GetID() { return ID; }

    public static Accessories StandardAccessories()
    {
        Accessories SomeAccessories = new Accessories();

        SomeAccessories.Weight = 700;
        SomeAccessories.Width  = 160;
        SomeAccessories.Height = 150;
        SomeAccessories.Length = 200;

        SomeAccessories.aFirm = Firm.Volkswagen;
        SomeAccessories.aType = CarBodyType.tSedan;

        return SomeAccessories;
    }

    @Override
    public String toString()
    {
        return "Accessories#"+ID+"[ Firm:"+ aFirm + ", Type:" + aType.toString() +" ]";
    }

}