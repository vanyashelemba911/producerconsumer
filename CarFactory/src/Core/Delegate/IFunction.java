package Core.Delegate;

public interface IFunction<T1>
{
    T1 Invoke();
}