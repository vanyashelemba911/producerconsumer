package Core.Delegate;

public interface IAction<T1>
{
    void Invoke(T1 param);
}