package Core;

import Core.Details.Accessories;
import Core.Details.Body;
import Core.Details.Engine;
import Core.Logger.DefaultOut;
import Core.Logger.FileLoggerAsync;
import Core.Logger.ILogger;
import com.ThreadPool_pack.ThreadPool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class CarManufacturer implements IStartable
{
    private final static Object SyncWhenCreating = new Object();

    private static volatile int _id = 0;
    private int ID;

    private int CarMakingTime;
    private int ConveyorCount;
    private volatile int CarCount;

    private HashMap<Long,Float> ConveyorsProgress;
    private HashMap<Integer,ArrayList<Runnable>> ConveyorNotifications;

    private ArrayList<Runnable> NotificationCommands;
    private ArrayList<Runnable> NotificationWhenSupply;

    private Storage<Car> CarStorage;
    private Storage<Body> BodyStorage;
    private Storage<Engine> EngineStorage;
    private Storage<Accessories> AccessoriesStorage;

    private ThreadPool Conveyors;

    private ILogger Log;

    private SimulationState CurrentState;

    private Object WhenPause;

    //region Constructors

    //-----------------------
    public CarManufacturer(
            Storage<Body> bodyStorage, Storage<Engine> engineStorage,
            Storage<Accessories> accessoriesStorage, Storage<Car> carStorage)
    {
        Initialize(bodyStorage, engineStorage, accessoriesStorage, carStorage, 4, 500);
    }
    //-----------------------
    public CarManufacturer(
            Storage<Body> bodyStorage, Storage<Engine> engineStorage,
            Storage<Accessories> accessoriesStorage, Storage<Car> carStorage,
            int Conveyors)
    {
        Initialize(bodyStorage, engineStorage, accessoriesStorage, carStorage, Conveyors, 500);
    }
    //-----------------------
    public CarManufacturer(
            Storage<Body> bodyStorage, Storage<Engine> engineStorage,
            Storage<Accessories> accessoriesStorage, Storage<Car> carStorage,
            int Conveyors,
            int MakingTime)
    {
        Initialize(bodyStorage, engineStorage, accessoriesStorage, carStorage, Conveyors, MakingTime);
    }
    //-----------------------

    //endregion

    //region Init
    private void Initialize(
            Storage<Body> bodyStorage, Storage<Engine> engineStorage,
            Storage<Accessories> accessoriesStorage, Storage<Car> carStorage,
            int ConveyorsNum,
            int MakingTime)
    {
        if( engineStorage == null
            || bodyStorage == null
            || accessoriesStorage == null
            || carStorage == null
            || ConveyorsNum < 1
            || MakingTime < 0 ) throw new IllegalArgumentException();

        synchronized (SyncWhenCreating)
        {
            ID = _id++;
        }

        CarCount = 0;

        CarStorage = carStorage;
        BodyStorage = bodyStorage;
        EngineStorage = engineStorage;
        AccessoriesStorage = accessoriesStorage;

        Conveyors = new ThreadPool(ConveyorsNum);

        ConveyorCount = ConveyorsNum;

        CarMakingTime = MakingTime;

        ConveyorsProgress = new HashMap<Long,Float>();

        NotificationCommands = new ArrayList<Runnable>();
        NotificationWhenSupply = new ArrayList<>();

        ConveyorNotifications = new HashMap<>();

        WhenPause = new Object();

        CurrentState = SimulationState.ssStopped;
    }
    //endregion

    //region LogWriter
    private void WriteToLog(String msg)
    {
        if( Log != null )
            Log.Write(msg);
        else
            DefaultOut.Write( msg );
    }

    //endregion

    //region AdditionalMethods
    private void OnPropertyChange( ArrayList<Runnable> commands )
    {
        for (Runnable notificationCommand : commands) notificationCommand.run();
    }
    //endregion

    //region ManufacturerLogic

    private Car MakeACar(Body body, Engine engine, Accessories accessories) throws Exception
    {
        int Ticks = 10;

        int DetailMakingTick = CarMakingTime/Ticks;

        float Progress = 0.0F;

        ConveyorsProgress.put(Thread.currentThread().getId(),Progress);

        Integer ConveyorID = 0;

        for(Long someKey : ConveyorsProgress.keySet())
        {
            if( someKey == Thread.currentThread().getId() )
                break;
            ConveyorID++;
        }

        for(int i = 0; i < Ticks; i++)
        {
            if( CurrentState == SimulationState.ssPaused )
            {
                synchronized(WhenPause)
                {
                    WhenPause.wait();
                }
            }

            Progress += 100.0F / Ticks;

            ConveyorsProgress.put(Thread.currentThread().getId(),Progress);

            if( this.ConveyorNotifications.get(ConveyorID) != null )
                OnPropertyChange(this.ConveyorNotifications.get(ConveyorID));

            Thread.sleep(DetailMakingTick);
        }

        ConveyorsProgress.put(Thread.currentThread().getId(), 0.0F);

        return new Car(body,engine,accessories);
    }

    private void ConveyorWork()
    {
        try
        {
            Body SomeBody = BodyStorage.TakeItem();
            Engine SomeEngine = EngineStorage.TakeItem();
            Accessories SomeAccessories = AccessoriesStorage.TakeItem();

            Car SomeCar = MakeACar(SomeBody,SomeEngine,SomeAccessories);

            synchronized (SyncWhenCreating) {CarCount++;}

            CarStorage.PutItem(SomeCar);

            OnPropertyChange(NotificationWhenSupply);
        }
        catch(Exception exc)
        {
            WriteToLog( "Exception in ConveyorWork()" + exc.getMessage());
        }
    }
    //endregion

    //region Interface
    public void MakeAnOrders(int OrdersNum) throws Exception
    {
        if(OrdersNum < 0) throw new IllegalArgumentException("OrdersNum < 0");

        for(int i = 0; i < OrdersNum; i++)
        {
            Conveyors.InsertCommandIntoQueue(this::ConveyorWork);
        }

        OnPropertyChange(NotificationCommands);
    }

    public int GetCarCount(){ return CarCount; }
    public int GetManufacturerID(){ return ID; }
    public int OrdersCount() { return Conveyors.GetCommandsCount(); }
    public int GetConveyorCount(){ return ConveyorCount; }

    public void NotifyWhenSupply( Runnable command )
    {
        if(command == null) throw new IllegalArgumentException("Manufacturer: command == null");

        NotificationWhenSupply.add(command);
    }

    public void NotifyWhenMakeOrders(Runnable Command)
    {
        NotificationCommands.add(Command);
    }

    public void SetDebugLog( ILogger log )
    {
        if( log == null )
            throw new IllegalArgumentException("SetDebugLog()");

        Log = log;
    }

    public float GetProgresses(int conveyorNum)
    {
        if( ConveyorsProgress.containsKey(ConveyorsProgress.keySet().toArray()[conveyorNum]) )
            return ConveyorsProgress.get( ConveyorsProgress.keySet().toArray()[conveyorNum] );
        else
            return 0.0F;
    }

    public void SetNotifyWhenWork( int conveyorNum, Runnable command )
    {
        if( !ConveyorNotifications.containsKey(conveyorNum) )
        {
            ConveyorNotifications.put(conveyorNum, new ArrayList<>());
        }

        ConveyorNotifications.get( conveyorNum ).add(command);
    }

    public void SetCarMakingTime( int time  )
    {
        if( time < 0 ) throw new IllegalArgumentException("SetCarMakingTime < 0 )");

        this.CarMakingTime = time;
    }

    @Override
    public void Start()
    {
        if( CurrentState == SimulationState.ssStopped )
        {
            Conveyors.StartWork();

            CurrentState = SimulationState.ssRunning;
        }
    }

    @Override
    public void Pause()
    {
        if( CurrentState == SimulationState.ssRunning )
        {
            CurrentState = SimulationState.ssPaused;
        }
    }

    @Override
    public void Resume()
    {
        if( CurrentState == SimulationState.ssPaused )
        {
            CurrentState = SimulationState.ssRunning;

            synchronized(WhenPause)
            {
                WhenPause.notifyAll();
            }
        }
    }

    @Override
    public void Stop()
    {
        if( CurrentState != SimulationState.ssStopped )
        {
            Conveyors.FinishWork();

            CurrentState = SimulationState.ssStopped;
        }
    }
    //endregion
}