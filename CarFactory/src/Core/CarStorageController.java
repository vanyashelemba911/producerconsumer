package Core;

import Core.Delegate.IAction;
import Core.Delegate.IFunction;
import Core.Logger.DefaultOut;
import Core.Logger.ILogger;

import java.util.Random;

public class CarStorageController
{
    IAction<Integer> MakeAnOrder;
    IFunction<Integer> GetStorageSize;
    IFunction<Integer> GetStorageMaxSize;
    IFunction<Integer> OrdersCount;

    private ILogger Log;

    //region Ctor
    public CarStorageController( IAction<Integer> makeAnOrder, IFunction<Integer> storageSizeCheck, IFunction<Integer> storageMaxSizeCheck, IFunction<Integer> ordersCount )
    {
        if( makeAnOrder == null || storageSizeCheck == null || storageMaxSizeCheck == null )
            throw new ExceptionInInitializerError("new CarStorageController(null);");

        OrdersCount = ordersCount;
        MakeAnOrder = makeAnOrder;
        GetStorageSize = storageSizeCheck;
        GetStorageMaxSize = storageMaxSizeCheck;
    }

    //endregion

    //region LogWriter
    private void WriteToLog(String msg)
    {
        if( Log != null )
            Log.Write(msg);
        else
            DefaultOut.Write( msg );
    }

    //endregion

    //region Interface
    public void SetDebugLog( ILogger log )
    {
        if( log == null )
            throw new IllegalArgumentException("SetDebugLog()");

        Log = log;
    }

    public void MakeSomeOrder()
    {
        if( OrdersCount.Invoke() > GetStorageMaxSize.Invoke() - 2 ) return;

        int StorageSize = GetStorageSize.Invoke();
        int StorageMaxSize = GetStorageMaxSize.Invoke();

        if(StorageSize < StorageMaxSize)
        {
            Random rand = new Random();

            int OrderNum = rand.nextInt(StorageMaxSize - StorageSize)/3 + 1;

            MakeAnOrder.Invoke(OrderNum);

            WriteToLog("CarStorageController make an orders: " + OrderNum);
        }
    }

    //endregion
}