package Core.Factories;

import Core.Details.Detail;

public interface IDetailFactory
{
    public Detail Generate(int seed);
}