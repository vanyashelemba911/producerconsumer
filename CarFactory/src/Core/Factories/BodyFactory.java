package Core.Factories;

import Core.Details.Body;
import Core.Details.Enums.CarBodyType;
import Core.Details.Detail;
import Core.Details.Enums.Color;

import java.util.HashMap;

public class BodyFactory implements IDetailFactory
{
    @Override
    public Detail Generate(int seed)
    {
        Body SomeBody = Body.StandardBody();

        SomeBody.bColor = Color.values()[seed % Color.values().length];
        SomeBody.bType  = CarBodyType.values()[seed % CarBodyType.values().length];

        SomeBody.Weight = ((seed % 100) + (float)((seed % 1000) / 10) / 100 ) + 500;
        SomeBody.Length = ((seed % 1000) / 10 + (float)(seed / 100) / 100 ) + 150;
        SomeBody.Width  = (seed / 100 + (float)((seed % 10) * 10 + (seed / 1000)) / 100.0F ) + 130;
        SomeBody.Height = ((seed % 10) * 10 + (seed / 1000) + (float)((seed % 100) / 100.0) ) + 120;

        return SomeBody;
    }

    @Override
    public String toString()
    {
        return "Body factory";
    }
}