package Core.Factories;

import Core.Details.Accessories;
import Core.Details.Detail;
import Core.Details.Engine;
import Core.Details.Enums.CarBodyType;
import Core.Details.Enums.EngineType;
import Core.Details.Enums.Firm;
import Core.Details.Enums.FuelType;

public class AccessoriesFactory implements IDetailFactory
{
    @Override
    public Detail Generate(int seed)
    {
        Accessories SomeAccessories = Accessories.StandardAccessories();

        SomeAccessories.aFirm = Firm.values()[ seed % Firm.values().length ];
        SomeAccessories.aType = CarBodyType.values()[ seed % Firm.values().length ];

        SomeAccessories.Weight = ((seed % 100) + (float)((seed % 1000) / 10) / 100 ) + 500;
        SomeAccessories.Length = ((seed % 1000) / 10 + (float)(seed / 100) / 100 ) + 150;
        SomeAccessories.Width  = (seed / 100 + (float)((seed % 10) * 10 + (seed / 1000)) / 100.0F ) + 130;
        SomeAccessories.Height = ((seed % 10) * 10 + (seed / 1000) + (float)((seed % 100) / 100.0) ) + 120;

        return SomeAccessories;
    }

    @Override
    public String toString()
    {
        return "Accessories factory";
    }
}

