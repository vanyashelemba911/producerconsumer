package Core.Factories;

import Core.Details.Detail;
import Core.Details.Engine;
import Core.Details.Enums.EngineType;
import Core.Details.Enums.FuelType;

public class EngineFactory implements IDetailFactory
{
    @Override
    public Detail Generate(int seed)
    {
        Engine SomeEngine = Engine.StandardEngine();

        SomeEngine.eFuel = FuelType.values()[seed % FuelType.values().length];
        SomeEngine.eType = EngineType.values()[seed % EngineType.values().length];

        SomeEngine.Weight = ((seed % 100) + (float)((seed % 1000) / 10) / 100 ) + 500;
        SomeEngine.Length = ((seed % 1000) / 10 + (float)(seed / 100) / 100 ) + 150;
        SomeEngine.Width  = (seed / 100 + (float)((seed % 10) * 10 + (seed / 1000)) / 100.0F ) + 130;
        SomeEngine.Height = ((seed % 10) * 10 + (seed / 1000) + (float)((seed % 100) / 100.0) ) + 120;

        return SomeEngine;
    }

    @Override
    public String toString()
    {
        return "Engine factory";
    }
}