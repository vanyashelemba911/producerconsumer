package Core.Emulator_pack;

public interface IEmulator
{
    void WhenPutToStorage(String storageName,  Runnable command);
    void WhenTakeFromStorage(String storageName,  Runnable command);
    int StorageMaxSize(String storageName);
    int StorageCurrentSize(String storageName);

    int GetSupplierNum(String suppliersName);
    float GetSupplierProgress( String suppliersName, int supplierNum );
    void SetWhenCreatingSupplier(String suppliersName, int supplierNum, Runnable command);

    void WhenManufacturerConveyorWork( int conveyorNum, Runnable command );
    float ManufacturerConveyorProgress( int conveyorNum );
    void NotifyWhenManufacturerMakeOrders( Runnable command );
    int ManufacturerOrders();
    int ManufacturerFinishedCarsCount();
    void NotifyWhenManufacturerSupply( Runnable command );

    int ManufacturerConveyorCount();

    void SetDealerRequestRate(int rate);
    int GetDealersCount();
    int GetDealersOrders(int dealer);
    void WhenDealerBought( int dealer , Runnable command );

    void SetCarMakingTime(int time);
    void SetBodyMakingTime(int time);
    void SetEngineMakingTime(int time);
    void SetAccessoriesMakingTime(int time);
}