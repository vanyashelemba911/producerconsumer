package Core.Emulator_pack;

import Core.*;
import Core.Delegate.IAction;
import Core.Details.Body;
import Core.Details.Engine;
import Core.Details.Accessories;
import Core.Factories.AccessoriesFactory;
import Core.Factories.BodyFactory;
import Core.Factories.EngineFactory;
import Core.Logger.DefaultOut;
import Core.Logger.ILogger;

import java.util.ArrayList;
import java.util.HashMap;

public class Emulator implements IEmulator, IStartable
{
    private ILogger Log;

    private Config CurrentConfig;

    private ArrayList<Dealer> Dealers;
    private HashMap<String, Storage> Storages;
    private CarManufacturer CurrentCarManufacturer;
    private HashMap<String, ArrayList<Supplier>> Suppliers;
    private CarStorageController CurrentCarStorageController;

    private SimulationState CurrentState;

    //region Ctor
    public Emulator(ILogger log)
    {
        Initialize(Config.GetStandardConfig(),log);
    }
    public Emulator(Config currentConfig, ILogger log)
    {
        Initialize(currentConfig,log);
    }
    //endregion

    //region Init
    private void Initialize(Config currentConfig, ILogger log)
    {
        if(currentConfig == null || log == null) throw new IllegalArgumentException("Emulator Initialize currentConfig == null");

        Log = log;

        CurrentConfig = currentConfig;

        CurrentState = SimulationState.ssStopped;

        Storages = new HashMap<>();
        Suppliers = new HashMap<>();

        InitializeEntities();
    }
    //endregion

    //region LogWriter
    private void WriteToLog(String msg)
    {
        if( Log != null )
            Log.Write(msg);
        else
            DefaultOut.Write( msg );
    }

    //endregion

    //region Logic

    //region Initializers
    private void InitializeStorages()
    {
        Storages.put("CarStorage", new Storage<Car>(CurrentConfig.CarStorageSize,Car.class));
        Storages.put("BodyStorage", new Storage<Body>(CurrentConfig.BodyStorageSize,Body.class));
        Storages.put("EngineStorage", new Storage<Engine>(CurrentConfig.EngineStorageSize,Engine.class));
        Storages.put("AccessoriesStorage", new Storage<Accessories>(CurrentConfig.AccessoriesStorageSize,Accessories.class));
    }

    private void InitializeSuppliers()
    {
        Suppliers.put("BodySuppliers", new ArrayList<>(CurrentConfig.BodySupplierNum));
        for(int i = 0; i < CurrentConfig.BodySupplierNum; i++)
        {
            Supplier<Body> SomeSupplier = new Supplier<Body>(Storages.get("BodyStorage"), new BodyFactory(), CurrentConfig.BodyMakingTime);

            SomeSupplier.SetDebugLog(Log);

            Suppliers.get("BodySuppliers").add(SomeSupplier);
        }

        Suppliers.put("EngineSuppliers", new ArrayList<>(CurrentConfig.EngineSupplierNum));
        for(int i = 0; i < CurrentConfig.EngineSupplierNum; i++)
        {
            Supplier<Engine> SomeSupplier = new Supplier<Engine>(Storages.get("EngineStorage"), new EngineFactory(), CurrentConfig.EngineMakingTime);

            SomeSupplier.SetDebugLog(Log);

            Suppliers.get("EngineSuppliers").add(SomeSupplier);
        }

        Suppliers.put("AccessoriesSuppliers", new ArrayList<>(CurrentConfig.AccessoriesSupplierNum));
        for(int i = 0; i < CurrentConfig.AccessoriesSupplierNum; i++)
        {
            Supplier<Accessories> SomeSupplier = new Supplier<Accessories>(Storages.get("AccessoriesStorage"), new AccessoriesFactory(), CurrentConfig.AccessoriesMakingTime);

            SomeSupplier.SetDebugLog(Log);

            Suppliers.get("AccessoriesSuppliers").add(SomeSupplier);
        }
    }

    private void InitializeDealers()
    {
        Dealers = new ArrayList<Dealer>(CurrentConfig.DealersNum);
        for(int i = 0; i < CurrentConfig.DealersNum; i++)
        {
            Dealer SomeDealer = new Dealer(Storages.get("CarStorage"),CurrentConfig.DealerRequestRate);

            SomeDealer.SetDebugLog(Log);

            Dealers.add( SomeDealer );
        }
    }

    private void InitializeManufacturer()
    {
        CurrentCarManufacturer = new CarManufacturer(
                Storages.get("BodyStorage"),
                Storages.get("EngineStorage"),
                Storages.get("AccessoriesStorage"),
                Storages.get("CarStorage"),
                CurrentConfig.ManufacturerConveyorsNum,
                CurrentConfig.CarMakingTime);

        CurrentCarManufacturer.SetDebugLog(Log);
    }

    private void InitializeCarStorageController()
    {
        IAction<Integer> MakeAnOrderCommand = (Integer param) ->
        {
            try {CurrentCarManufacturer.MakeAnOrders(param);}
            catch (Exception e) { WriteToLog("MakeAnOrderCommand: " + e.getMessage()); }
        };

        CurrentCarStorageController = new CarStorageController
                (
                        MakeAnOrderCommand,
                        () -> Storages.get("CarStorage").GetItemCount(),
                        () -> Storages.get("CarStorage").GetMaxSize(),
                        () -> CurrentCarManufacturer.OrdersCount()
                );

        CurrentCarStorageController.SetDebugLog(Log);

        Storages.get("CarStorage").SubscribeForUpdatesWhenTake( ()->CurrentCarStorageController.MakeSomeOrder() );
    }

    //endregion

    private void InitializeEntities()
    {
        WriteToLog( "Initializing Storages..." );
        InitializeStorages();
        WriteToLog( "Initialized" );

        WriteToLog( "Initializing Suppliers..." );
        InitializeSuppliers();
        WriteToLog( "Initialized" );

        WriteToLog( "Initializing Dealers..." );
        InitializeDealers();
        WriteToLog( "Initialized" );

        WriteToLog( "Initializing Manufacturer..." );
        InitializeManufacturer();
        WriteToLog( "Initialized" );

        WriteToLog( "Initializing CarStorageController..." );
        InitializeCarStorageController();
        WriteToLog( "Initialized" );
    }

    private void WriteResultToLog()
    {
        Log.Write( "Simulation result:" );

        Log.Write( "Suppliers: " );
        for( int i = 0; i < Suppliers.get("BodySuppliers").size(); i++ )
        {
            Log.Write( "BodySupplier#" + Suppliers.get("BodySuppliers").get(i).GetID() + ": Finished - " + Suppliers.get("BodySuppliers").get(i).GetFinishedDetailsCount() + " Details" );
        }

        for( Supplier SomeSupplier : Suppliers.get("EngineSuppliers") )
        {
            Log.Write( "EngineSupplier#" + SomeSupplier.GetID() + ": Finished - " + SomeSupplier.GetFinishedDetailsCount() + " Details" );
        }

        for( Supplier SomeSupplier : Suppliers.get("AccessoriesSuppliers") )
        {
            Log.Write( "AccessoriesSupplier#" + SomeSupplier.GetID() + ": Finished - " + SomeSupplier.GetFinishedDetailsCount() + " Details" );
        }

        Log.Write( "Storages: " );
        Log.Write( "Storage<Car>: " + Storages.get("CarStorage").GetItemCount() + " / " + Storages.get("CarStorage").GetMaxSize() );
        Log.Write( "Storage<Body>: " + Storages.get("BodyStorage").GetItemCount() + " / " + Storages.get("BodyStorage").GetMaxSize() );
        Log.Write( "Storage<Engine>: " + Storages.get("EngineStorage").GetItemCount() + " / " + Storages.get("EngineStorage").GetMaxSize() );
        Log.Write( "Storage<Accessories>: " + Storages.get("AccessoriesStorage").GetItemCount() + " / " + Storages.get("AccessoriesStorage").GetMaxSize() );

        Log.Write( "Manufacturer: " );
        Log.Write( "CarManufacturer: finished " + CurrentCarManufacturer.GetCarCount() + " cars" );

        Log.Write( "Dealers: " );
        for( Dealer SomeDealer : Dealers )
        {
            Log.Write( "Dealer#" + SomeDealer.GetID() + " : bought " + SomeDealer.GetBoughtCarsCount() + " cars" );
        }

        Log.Write( "Simulation result is displayed." );
    }

    private void StartWork()
    {
        WriteToLog( "Starting BodySuppliers..." );
        for(int i = 0; i < CurrentConfig.BodySupplierNum; i++)
            Suppliers.get("BodySuppliers").get(i).Start();

        WriteToLog( "Starting EngineSuppliers..." );
        for(int i = 0; i < CurrentConfig.EngineSupplierNum; i++)
            Suppliers.get("EngineSuppliers").get(i).Start();

        WriteToLog( "Starting AccessoriesSuppliers..." );
        for(int i = 0; i < CurrentConfig.AccessoriesSupplierNum; i++)
            Suppliers.get("AccessoriesSuppliers").get(i).Start();

        for(int i = 0; i < CurrentConfig.DealersNum; i++)
            Dealers.get(i).Start();

        WriteToLog( "Starting CarManufacturer..." );
        CurrentCarManufacturer.Start();

        WriteToLog( "Making SomeOrder..." );
        CurrentCarStorageController.MakeSomeOrder();
    }

    private void StopWork()
    {
        Storages.get("CarStorage").Reinitialize();
        Storages.get("BodyStorage").Reinitialize();
        Storages.get("EngineStorage").Reinitialize();
        Storages.get("AccessoriesStorage").Reinitialize();

        WriteToLog( "Stopping BodySuppliers..." );
        for(int i = 0; i < CurrentConfig.BodySupplierNum; i++)
            Suppliers.get("BodySuppliers").get(i).Stop();

        WriteToLog( "Stopping EngineSuppliers..." );
        for(int i = 0; i < CurrentConfig.EngineSupplierNum; i++)
            Suppliers.get("EngineSuppliers").get(i).Stop();

        WriteToLog( "Stopping AccessoriesSuppliers..." );
        for(int i = 0; i < CurrentConfig.AccessoriesSupplierNum; i++)
            Suppliers.get("AccessoriesSuppliers").get(i).Stop();

        WriteToLog( "Stopping Dealers..." );
        for(int i = 0; i < CurrentConfig.DealersNum; i++)
            Dealers.get(i).Stop();

        WriteToLog( "Stopping CarManufacturer..." );
        CurrentCarManufacturer.Stop();
    }

    private void PauseWork()
    {
        WriteToLog( "Pause BodySuppliers..." );
        for(int i = 0; i < CurrentConfig.BodySupplierNum; i++)
            Suppliers.get("BodySuppliers").get(i).Pause();

        WriteToLog( "Pause EngineSuppliers..." );
        for(int i = 0; i < CurrentConfig.EngineSupplierNum; i++)
            Suppliers.get("EngineSuppliers").get(i).Pause();

        WriteToLog( "Pause AccessoriesSuppliers..." );
        for(int i = 0; i < CurrentConfig.AccessoriesSupplierNum; i++)
            Suppliers.get("AccessoriesSuppliers").get(i).Pause();

        WriteToLog( "Pause Dealers..." );
        for(int i = 0; i < CurrentConfig.DealersNum; i++)
            Dealers.get(i).Pause();

        WriteToLog( "Pause CarManufacturer..." );
        CurrentCarManufacturer.Pause();
    }

    private void ResumeWork()
    {
        for(int i = 0; i < CurrentConfig.BodySupplierNum; i++)
            Suppliers.get("BodySuppliers").get(i).Resume();

        for(int i = 0; i < CurrentConfig.EngineSupplierNum; i++)
            Suppliers.get("EngineSuppliers").get(i).Resume();

        for(int i = 0; i < CurrentConfig.AccessoriesSupplierNum; i++)
            Suppliers.get("AccessoriesSuppliers").get(i).Resume();

        for(int i = 0; i < CurrentConfig.DealersNum; i++)
            Dealers.get(i).Resume();

        CurrentCarManufacturer.Resume();
    }

    //endregion

    //region StorageInterface

    @Override
    public void WhenPutToStorage(String storageName,  Runnable command)
    {
        Storages.get(storageName).SubscribeForUpdatesWhenPut(command);
    }
    @Override
    public void WhenTakeFromStorage(String storageName,  Runnable command)
    {
        Storages.get(storageName).SubscribeForUpdatesWhenTake(command);
    }

    @Override public int StorageMaxSize(String storageName)
    {
        return Storages.get(storageName).GetMaxSize();
    }
    @Override public int StorageCurrentSize(String storageName)
    {
        return Storages.get(storageName).GetItemCount();
    }

    //endregion

    //region Suppliers

    @Override
    public int GetSupplierNum(String suppliersName)
    {
        return this.Suppliers.get(suppliersName).size();
    }

    @Override
    public float GetSupplierProgress( String suppliersName, int supplierNum )
    {
        return this.Suppliers.get(suppliersName).get(supplierNum).GetMakingProgress();
    }

    @Override
    public void SetWhenCreatingSupplier(String suppliersName, int supplierNum, Runnable command)
    {
        this.Suppliers.get(suppliersName).get(supplierNum).WhenCreating(command);
    }

    //endregion

    //region Manufacturer
    @Override
    public void WhenManufacturerConveyorWork( int conveyorNum, Runnable command )
    {
        CurrentCarManufacturer.SetNotifyWhenWork(conveyorNum,command);
    }

    @Override
    public float ManufacturerConveyorProgress( int conveyorNum )
    {
        return CurrentCarManufacturer.GetProgresses(conveyorNum);
    }

    @Override
    public void NotifyWhenManufacturerMakeOrders( Runnable command )
    {
        CurrentCarManufacturer.NotifyWhenMakeOrders(command);
    }

    @Override
    public int ManufacturerOrders(  )
    {
        return CurrentCarManufacturer.OrdersCount();
    }

    @Override
    public int ManufacturerConveyorCount()
    {
        return CurrentCarManufacturer.GetConveyorCount();
    }
    //endregion

    //region Setters
    @Override public void SetDealerRequestRate(int rate)
    {
        for( Dealer SomeDealer : Dealers ) SomeDealer.SetRequestRate( rate );
    }

    @Override public int GetDealersCount()
    {
        return Dealers.size();
    }

    @Override public int GetDealersOrders(int dealer)
    {
        return Dealers.get(dealer).GetBoughtCarsCount();
    }
    @Override public void WhenDealerBought( int dealer , Runnable command )
    {
        Dealers.get(dealer).NotifyWhenBuy(command);
    }

    @Override public int ManufacturerFinishedCarsCount()
    {
        return CurrentCarManufacturer.GetCarCount();
    }

    @Override public void NotifyWhenManufacturerSupply( Runnable command )
    {
        CurrentCarManufacturer.NotifyWhenSupply(command);
    }

    @Override public void SetCarMakingTime(int time)
    {
        CurrentCarManufacturer.SetCarMakingTime(time);
    }

    @Override public void SetBodyMakingTime(int time)
    {
        for( Supplier<Body> SomeSupplier : this.Suppliers.get("BodySuppliers"))
        {
            SomeSupplier.SetMakingTime( time );
        }
    }

    @Override public void SetEngineMakingTime(int time)
    {
        for( Supplier<Engine> SomeSupplier : this.Suppliers.get("EngineSuppliers"))
        {
            SomeSupplier.SetMakingTime( time );
        }
    }

    @Override public void SetAccessoriesMakingTime(int time)
    {
        for( Supplier<Accessories> SomeSupplier : this.Suppliers.get("AccessoriesSuppliers"))
        {
            SomeSupplier.SetMakingTime( time );
        }
    }

    //endregion

    //region IStartable

    @Override
    public void Start()
    {
        if(CurrentState == SimulationState.ssStopped)
        {
            StartWork();

            CurrentState = SimulationState.ssRunning;
        }
    }

    @Override
    public void Pause()
    {
        if(CurrentState == SimulationState.ssRunning)
        {
            PauseWork();

            CurrentState = SimulationState.ssPaused;
        }
    }

    @Override
    public void Resume()
    {
        if(CurrentState == SimulationState.ssPaused)
        {
            ResumeWork();

            CurrentState = SimulationState.ssRunning;
        }
    }

    @Override
    public void Stop()
    {
        if(CurrentState != SimulationState.ssStopped)
        {
            if(CurrentState != SimulationState.ssPaused)
                Pause();

            WriteResultToLog();

            StopWork();

            CurrentState = SimulationState.ssStopped;
        }
    }
    //endregion

}