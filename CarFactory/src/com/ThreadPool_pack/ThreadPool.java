package com.ThreadPool_pack;

import Core.Logger.DefaultOut;
import Core.Logger.ILogger;
import Core.SimulationState;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ThreadPool
{
    private int ThreadNum;

    private ArrayList<Thread> Threads; // Список потоків в пулі
    private ConcurrentLinkedQueue<Runnable> CommandQueue; // Черга команд які треба виконати

    private ILogger Log;

    //region Constructor
    public ThreadPool()
    {
        Initialize(4);
    }
    public ThreadPool(int threadNum)
    {
        Initialize(threadNum);
    }
    //endregion

    //region Initialize
    private void Initialize(int threadNum)
    {
        if(threadNum < 0) throw new IllegalArgumentException("threadNum < 0 in ThreadPool Initialize");

        CommandQueue = new ConcurrentLinkedQueue<>();

        Threads = new ArrayList<>(threadNum);

        ThreadNum = threadNum;
    }
    //endregion

    //region LogWriter
    private void WriteToLog(String msg)
    {
        if( Log != null )
            Log.Write(msg);
        else
            DefaultOut.Write( msg );
    }

    //endregion

    // region WorkingThreadsMethod
    // Суть - потік спить поки в чергу команд не закинуть щось,
    // коли закинули команду він отримує сповіщення - виходить з сну і виконує її
    // дана дія повторюється поки Пул потоків Живий
    private void WorkingThreadsMethod()
    {
        Runnable SomeCommand;

        try
        {
            while(true)
            {
                if(CommandQueue.isEmpty())
                {
                    synchronized(CommandQueue)
                    {
                        CommandQueue.wait();
                    }
                }

                SomeCommand = CommandQueue.poll();

                SomeCommand.run();
            }
        }
        catch(Exception exc)
        {
            WriteToLog(exc.getMessage());
        }
    }
    //endregion

    // region Інтерфейс для роботи з пулом

    public int GetCommandsCount()
    {
        return CommandQueue.size();
    }

    public void SetDebugLog( ILogger log )
    {
        if( log == null )
            throw new IllegalArgumentException("SetDebugLog()");

        Log = log;
    }

    public void InsertCommandIntoQueue(Runnable SomeCommand) throws Exception
    {
        if(SomeCommand == null) throw new NullPointerException("trying to RunCommand(null); !!!!");

        synchronized(CommandQueue)
        {
            CommandQueue.offer(SomeCommand);
            CommandQueue.notify();
        }
    }

    public void StartWork()
    {
        for(int i = 0; i < ThreadNum; i++)
        {
            Threads.add(new Thread(this::WorkingThreadsMethod));
            Threads.get(i).start();
        }
    }

    public void FinishWork()
    {
        for (Thread thread : Threads)
        {
            thread.stop();
        }

        Threads.clear();

        CommandQueue.clear();
    }

    //endregion
    
}