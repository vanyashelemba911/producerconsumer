package View;


import Controllers.MainViewController;
import javafx.application.Application;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application
{
    public Stage primaryStage;

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        try
        {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("sample.fxml"));

            Parent root = loader.load();
            this.primaryStage = primaryStage;
            Scene scene = new Scene(root, primaryStage.getWidth(),primaryStage.getHeight());

            primaryStage.setTitle("Car Factory");
            primaryStage.setScene(scene);
            primaryStage.show();

            MainViewController controller = loader.getController();

            primaryStage.setOnCloseRequest(controller.getCloseEventHandler());
        }
        catch(Exception exc)
        {

        }
    }
    public static void main(String[] args) {
        launch(args);
    }
}