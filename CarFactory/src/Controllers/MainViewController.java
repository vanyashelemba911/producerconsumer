package Controllers;

import Core.Config;
import Core.Emulator_pack.Emulator;
import Core.Logger.ConsoleLogger;
import Core.Logger.DefaultOut;
import Core.Logger.FileLoggerAsync;
import Core.Logger.ILogger;
import Core.SimulationState;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.File;
import java.util.HashMap;
import java.util.concurrent.Semaphore;


public class MainViewController
{
    //region XMLFields
    @FXML
    private Button StartButton;

    @FXML
    private Button StopButton;

    @FXML
    private Button ImportConfigButton;

    @FXML
    private Button SaveConfigButton;

    @FXML
    private TextField EngineSuppliersNum;

    @FXML
    private TextField BodySuppliersNum;

    @FXML
    private TextField AccessoriesSuppliersNum;

    @FXML
    private TextField ConveyorsNum;

    @FXML
    private TextField DealersNum;

    @FXML
    private TextField DealersRate;

    @FXML
    private TextField CarTime;

    @FXML
    private TextField EngineTime;

    @FXML
    private TextField BodyTime;

    @FXML
    private TextField AccessoriesTime;

    @FXML
    private TextField EngineStorageSize;

    @FXML
    private TextField BodyStorageSize;

    @FXML
    private TextField AccessoriesStorageSize;

    @FXML
    private TextField CarStorageSize;

    @FXML
    private Button ChangeCfgButton;

    @FXML
    private Button Pause;

    @FXML
    private Button Resume;

    @FXML
    private FlowPane BodyStorage;

    @FXML
    private FlowPane EngineStorage;

    @FXML
    private FlowPane CarStorage;

    @FXML
    private FlowPane AccessoriesStorage;

    @FXML
    private FlowPane BodySupplier;

    @FXML
    private FlowPane EngineSupplier;

    @FXML
    private FlowPane AccessoriesSupplier;

    @FXML
    private FlowPane Manufacturer;

    @FXML
    private Label BodyStStatus;

    @FXML
    private Label EngineStStatus;

    @FXML
    private Label AccessoriesStStatus;

    @FXML
    private Label CarStStatus;

    @FXML
    private Label ManufacturerStatus;

    @FXML
    private VBox StatusBox;
    //endregion

    //region Etc
    @FXML private void initialize()
    {
        onInitialize();
    }

    private javafx.event.EventHandler<WindowEvent> closeEventHandler = new javafx.event.EventHandler<WindowEvent>()
    {
        @Override
        public void handle(WindowEvent event)
        {
            onClose();
        }
    };

    private void ShowConfig()
    {
        EngineSuppliersNum.setText(String.valueOf(CurrentConfig.EngineSupplierNum));
        BodySuppliersNum.setText(String.valueOf(CurrentConfig.BodySupplierNum));
        AccessoriesSuppliersNum.setText(String.valueOf(CurrentConfig.AccessoriesSupplierNum));
        ConveyorsNum.setText(String.valueOf(CurrentConfig.ManufacturerConveyorsNum));
        DealersNum.setText(String.valueOf(CurrentConfig.DealersNum));
        DealersRate.setText(String.valueOf(CurrentConfig.DealerRequestRate));
        CarTime.setText(String.valueOf(CurrentConfig.CarMakingTime));
        EngineTime.setText(String.valueOf(CurrentConfig.EngineMakingTime));
        BodyTime.setText(String.valueOf(CurrentConfig.BodyMakingTime));
        AccessoriesTime.setText(String.valueOf(CurrentConfig.AccessoriesMakingTime));
        EngineStorageSize.setText(String.valueOf(CurrentConfig.EngineStorageSize));
        BodyStorageSize.setText(String.valueOf(CurrentConfig.BodyStorageSize));
        AccessoriesStorageSize.setText(String.valueOf(CurrentConfig.AccessoriesStorageSize));
        CarStorageSize.setText(String.valueOf(CurrentConfig.CarStorageSize));
    }

    private void ImportConfig()
    {
        Stage stage = new Stage();
        File CurrentDir = new File("Configs");
        FileChooser fileChooser = new FileChooser();

        if(!CurrentDir.exists()) CurrentDir.mkdir();

        stage.setTitle("Choose File");

        fileChooser.setInitialDirectory( CurrentDir );

        File SelectedFile = fileChooser.showOpenDialog(stage);

        if(SelectedFile != null)
            CurrentConfig = Config.ReadFromFile(SelectedFile.getPath());
        else
            CurrentConfig = Config.GetStandardConfig();
        ShowConfig();
    }

    private void ExportConfig()
    {
        Stage stage = new Stage();
        File CurrentDir = new File("Configs");
        FileChooser fileChooser = new FileChooser();

        if(!CurrentDir.exists()) CurrentDir.mkdir();

        stage.setTitle("Choose File");

        fileChooser.setInitialDirectory( CurrentDir );

        File SelectedFile = fileChooser.showSaveDialog( stage );

        if(SelectedFile != null)
            CurrentConfig.SaveToFile(SelectedFile.getPath());
    }

    public javafx.event.EventHandler<WindowEvent> getCloseEventHandler()
    {
        return closeEventHandler;
    }

    //endregion

    private Object AddCommand;
    private Object RemoveCommand;

    private Config CurrentConfig;
    private Emulator CurrentSimulation;

    private Semaphore UIUpdate;
    private Semaphore CommandExecLock;

    private HashMap<String, FlowPane> StorageView;

    private ILogger Log;

    private SimulationState CurrentState;

    //region LogWriter
    private void WriteToLog(String msg)
    {
        if( Log != null )
            Log.Write(msg);
        else
            DefaultOut.Write( msg );
    }

    //endregion

    //region Logic
    private void AddTo(String storageName)
    {
        try{ CommandExecLock.acquire(); } catch (Exception exc) {}

        FlowPane SomeStorageView = StorageView.get(storageName);

        File file = new File("Resources\\"+storageName+".png");

        Image image = new Image(file.toURI().toString());

        ImageView imageView = new ImageView();

        imageView.setImage(image);
        imageView.setFitWidth(55);
        imageView.setFitHeight(55);

        Platform.runLater(()-> {SomeStorageView.getChildren().add( imageView ); synchronized(AddCommand){ AddCommand.notify(); }});

        synchronized(AddCommand){ try{ AddCommand.wait(50); } catch (Exception exc) {} }

        CommandExecLock.release();

    }

    private void RemoveFrom(String storageName)
    {
        try{ CommandExecLock.acquire(); } catch (Exception exc) {}
        FlowPane SomeStorageView = StorageView.get(storageName);

        Platform.runLater(()->
        {
            SomeStorageView.getChildren().remove(SomeStorageView.getChildren().size()-1);
            synchronized(RemoveCommand){ RemoveCommand.notify(); }
        });

        synchronized(RemoveCommand){ try{ RemoveCommand.wait(50); } catch (Exception exc) {} }

        CommandExecLock.release();
    }

    private void onInitialize()
    {
        Log = new ConsoleLogger();

        WriteToLog("Initializing...");

        AddCommand = new Object();
        RemoveCommand = new Object();

        UIUpdate = new Semaphore(1);
        CommandExecLock = new Semaphore(1);

        CurrentConfig = Config.GetStandardConfig();

        WriteToLog(CurrentConfig.toString());

        ShowConfig();

        StorageView = new HashMap<>();

        CurrentState = SimulationState.ssStopped;

        StorageView.put("BodyStorage",BodyStorage);
        StorageView.put("EngineStorage",EngineStorage);
        StorageView.put("AccessoriesStorage",AccessoriesStorage);
        StorageView.put("CarStorage",CarStorage);

        WriteToLog("Initialized");
    }

    private void onClose()
    {
        if( CurrentSimulation != null )
            CurrentSimulation.Stop();

        WriteToLog("Exit");

        if( Log != null && Log instanceof FileLoggerAsync )
            ((FileLoggerAsync)Log).SaveLogToFile();
    }

    private void ClearInterface()
    {
        StorageView.get("BodyStorage").getChildren().clear();
        StorageView.get("EngineStorage").getChildren().clear();
        StorageView.get("AccessoriesStorage").getChildren().clear();
        StorageView.get("CarStorage").getChildren().clear();

        BodySupplier.getChildren().clear();
        EngineSupplier.getChildren().clear();
        AccessoriesSupplier.getChildren().clear();
        Manufacturer.getChildren().clear();

        StatusBox.getChildren().clear();
    }

    private void InitializeStorage( String name, Label statusLabel)
    {
        CurrentSimulation.WhenPutToStorage( name, () ->
        {
            AddTo(name);
            Platform.runLater(()->statusLabel.setText( CurrentSimulation.StorageCurrentSize(name) + "/" + CurrentSimulation.StorageMaxSize(name) ));
        } );
        CurrentSimulation.WhenTakeFromStorage( name, () ->
        {
            RemoveFrom(name);
            Platform.runLater( ()->statusLabel.setText( CurrentSimulation.StorageCurrentSize(name) + "/" + CurrentSimulation.StorageMaxSize(name) ) );
        } );
    }

    private void InitializeStorages()
    {
        InitializeStorage("CarStorage", CarStStatus);
        InitializeStorage("BodyStorage", BodyStStatus);
        InitializeStorage("EngineStorage", EngineStStatus);
        InitializeStorage("AccessoriesStorage", AccessoriesStStatus);

        CarStStatus.setText( "--/--" );
        BodyStStatus.setText( "--/--" );
        EngineStStatus.setText( "--/--" );
        AccessoriesStStatus.setText( "--/--" );
    }


    private void InitStatusBar()
    {
        for( int i = 0; i < CurrentSimulation.GetDealersCount(); i++ )
        {
            int I = i;
            Label SomeLabel = new Label( "Диллер №" + I + "Купив 0 авто");
            CurrentSimulation.WhenDealerBought( I, () -> Platform.runLater( () -> SomeLabel.setText("Диллер №" + I + "Купив "+ CurrentSimulation.GetDealersOrders(I) +" авто") ) );
            StatusBox.getChildren().add( SomeLabel );
        }
        Label SomeLabel = new Label( "Фабрика виготовила: 0 авто");

        CurrentSimulation.NotifyWhenManufacturerSupply( () -> Platform.runLater(
                ()->
                {
                    if(CurrentSimulation != null) SomeLabel.setText( "Фабрика виготовила: "+ CurrentSimulation.ManufacturerFinishedCarsCount() +" авто" );

                } ) );

        StatusBox.getChildren().add( SomeLabel );
    }

    private void InitializeSupplier(String supplierName, FlowPane view)
    {
        for(int i = 0; i < CurrentSimulation.GetSupplierNum(supplierName); i++)
        {
            ProgressIndicator SomeBar = new ProgressIndicator();

            int finalI = i;

            Runnable Command = () ->
            {
                try{ UIUpdate.acquire(); } catch (Exception exc){}

                SomeBar.setProgress( CurrentSimulation.GetSupplierProgress(supplierName, finalI) / 100.0 );

                UIUpdate.release();
            };

            SomeBar.setProgress( 0.0 );

            CurrentSimulation.SetWhenCreatingSupplier( supplierName, i, Command );

            view.getChildren().add( SomeBar );
        }
    }

    private void InitializeSuppliers()
    {
        InitializeSupplier( "BodySuppliers" , BodySupplier );
        InitializeSupplier( "EngineSuppliers" , EngineSupplier );
        InitializeSupplier( "AccessoriesSuppliers" , AccessoriesSupplier );
    }

    private void InitializeManufacturer()
    {
        for(int i = 0; i < CurrentSimulation.ManufacturerConveyorCount(); i++)
        {
            ProgressIndicator SomeBar = new ProgressIndicator();

            int finalI = i;

            Runnable Command = () -> Platform.runLater( () -> {
                try{ UIUpdate.acquire(); } catch (Exception exc){}
                SomeBar.setProgress( CurrentSimulation.ManufacturerConveyorProgress(finalI) / 100.0 );
                UIUpdate.release();
            } );

            SomeBar.setProgress( 0.0 );

            CurrentSimulation.WhenManufacturerConveyorWork(finalI,Command);
            CurrentSimulation.NotifyWhenManufacturerMakeOrders(  () -> Platform.runLater( () ->
                    {
                        try{ UIUpdate.acquire(); } catch (Exception exc){}
                        ManufacturerStatus.setText( "Замовлень у черзі: " + CurrentSimulation.ManufacturerOrders());
                        UIUpdate.release();
                    }
                    ));

            this.Manufacturer.getChildren().add( SomeBar );
        }
    }

    private void InitialPreparation()
    {
        if( Log == null )
            Log = new FileLoggerAsync();

        CurrentSimulation = new Emulator(CurrentConfig,Log);

        InitializeStorages();

        InitializeSuppliers();

        InitializeManufacturer();

        InitStatusBar();
    }

    private void Start()
    {
        if( CurrentState == SimulationState.ssStopped )
        {
            if( CurrentSimulation == null )
                InitialPreparation();

            CurrentSimulation.Start();

            CurrentState = SimulationState.ssRunning;

            WriteToLog("Simulation.Start");
        }
    }

    private void Stop()
    {
        if( CurrentState != SimulationState.ssStopped )
        {
            if(CurrentSimulation != null)
            {
                CurrentSimulation.Stop();
                CurrentSimulation = null;
            }

            CurrentState = SimulationState.ssStopped;

            WriteToLog("Simulation.Stop");

            ClearInterface();
        }
    }

    private void Resume()
    {
        if( CurrentState == SimulationState.ssPaused )
        {
            if(CurrentSimulation != null)
                CurrentSimulation.Resume();

            CurrentState = SimulationState.ssRunning;

            WriteToLog("Simulation.Resume");
        }
    }

    private void Pause()
    {
        if( CurrentState == SimulationState.ssRunning )
        {
            if(CurrentSimulation != null)
                CurrentSimulation.Pause();

            CurrentState = SimulationState.ssPaused;

            WriteToLog("Simulation.Pause");
        }
    }

    private void MakeChanges()
    {
        if( CurrentSimulation == null ) return;

        CurrentSimulation.SetCarMakingTime( CurrentConfig.CarMakingTime );
        CurrentSimulation.SetBodyMakingTime( CurrentConfig.BodyMakingTime );
        CurrentSimulation.SetEngineMakingTime( CurrentConfig.EngineMakingTime );
        CurrentSimulation.SetAccessoriesMakingTime( CurrentConfig.AccessoriesMakingTime );

        CurrentSimulation.SetDealerRequestRate( CurrentConfig.DealerRequestRate );
    }

    private void ChangeCfg()
    {
        CurrentConfig.BodyMakingTime = Integer.parseInt( BodyTime.getText() );
        CurrentConfig.EngineMakingTime = Integer.parseInt( EngineTime.getText() );
        CurrentConfig.AccessoriesMakingTime = Integer.parseInt(AccessoriesTime.getText());

        CurrentConfig.CarStorageSize = Integer.parseInt( CarStorageSize.getText() );
        CurrentConfig.BodyStorageSize = Integer.parseInt( BodyStorageSize.getText() );
        CurrentConfig.EngineStorageSize = Integer.parseInt( EngineStorageSize.getText() );
        CurrentConfig.AccessoriesStorageSize = Integer.parseInt( AccessoriesStorageSize.getText() );

        CurrentConfig.BodySupplierNum = Integer.parseInt( BodySuppliersNum.getText() );
        CurrentConfig.EngineSupplierNum = Integer.parseInt( EngineSuppliersNum.getText() );
        CurrentConfig.AccessoriesSupplierNum = Integer.parseInt( AccessoriesSuppliersNum.getText() );

        CurrentConfig.ManufacturerConveyorsNum = Integer.parseInt( ConveyorsNum.getText() );

        CurrentConfig.DealersNum = Integer.parseInt( DealersNum.getText() );

        CurrentConfig.CarMakingTime = Integer.parseInt( CarTime.getText() );

        CurrentConfig.DealerRequestRate = Integer.parseInt( DealersRate.getText() );

        MakeChanges();
    }

    //endregion

    //region Buttons
    public void OnStartClick(ActionEvent actionEvent)
    {
        Start();
    }

    public void OnPauseClick(ActionEvent actionEvent)
    {
        Pause();
    }

    public void OnResumeClick(ActionEvent actionEvent)
    {
        Resume();
    }

    public void OnStopClick(ActionEvent actionEvent)
    {
        Stop();
    }

    public void OnImportConfigClick(ActionEvent actionEvent)
    {
        ImportConfig();
    }

    public void OnSaveConfigClick(ActionEvent actionEvent)
    {
        ExportConfig();
    }

    public void OnChangeCfg(ActionEvent event) { ChangeCfg(); }

    //endregion
}
